<?php

class Home extends Controller {
  public function index() {
    $data['title'] = 'home';
    $data["nama"] = $this->model('User_model')->getUser();
    $this->view('template/header', $data);
    $this->view("home/index", $data);
    $this->view('template/footer', $data);
  }

  public function about($company = 'SMKN1')
  {
    $data["title"] = "About";
    $data["company"] = $company;
    $this->view("template/header");
  }

  public function register()
  {
    $data_view = [];

    if (isset($_POST['username'])) {
      $data = [
        'email' => $_POST['email'],
        'username' => $_POST['username'],
        'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
        'firstname' => $_POST['firstname'],
        'lastname' => $_POST['lastname'],
      ];

      $this->model('user_model')->addUser($data);

      Flasher::setFlash("register success", "silahkan login", "success");
      header('Location: http://localhost/phpmvc/public/home/login');
      exit();
}

    $this->view("auth/register", $data_view);

  }

  public function login() {
    if(isset($_POST['email'])) {
      $user=$this->model('user_model')->getUserByEmail($_POST['email']);
     if($user && password_verify($_POST['password'], $user['password'])) {
      header('Location: http://localhost/phpmvc/public/home/index');
      exit();
     }
    }

    $this->view("auth/login",[]);

  }
}